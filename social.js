const _ = require('lodash');
const fs = require('fs');
const OrientDBClient = require("orientjs").OrientDBClient;

const ORIENT_USER = 'root';
const ORIENT_PASS = 'rootpwd';
const ORIENT_HOST = 'localhost';
const ORIENT_PORT = 2424;

const DB_NAME = "twitter";

const PROCESSORS = 4;

let rIds = {};

async function asyncMap(array, callback) {
  let results = [];
  for (let index = 0; index < array.length; index++) {
    const result = await callback(array[index], index, array);
    results.push(result);
  }

  return results;
}

async function process(pool, lines = [], index=0) {
  const session = await pool.acquire();

  if (!session) {
    reject(new Error('Session error, session can not be undefined'));
    return;
  }

  console.log(`Processing batch ${index}`);

  async function createVertex(vertex) {
    if (vertex in rIds) return rIds[vertex];

    let params = {params: {id: vertex}};

    function getVertex(vertex) {
      return session.select().from('User')
      .where({id: vertex})
      .one();

      // .command("select from User where id = :id", params)
      // .one();
    }

    let v = await getVertex(vertex);

    if (_.isEmpty(v)) {
      try {
        v = await session.insert().into('User')
        .set({id: vertex})
        .one();
      } catch (e) {
        // console.log(e.message)
      } finally {
        v = await getVertex(vertex);
      }
    }

    if (_.isEmpty(v)) {
      reject(new Error('Error while fetching/creating vertex'));
      return;
    }

    const rid = v['@rid'];
    const {cluster, position} = rid;

    rIds[vertex] = `#${cluster}:${position}`;

    return rIds[vertex];
  }

  async function link(a, b) {
    return new Promise(async (resolve, reject) => {
      const rIdA = await createVertex(a);
      const rIdB = await createVertex(b);


      // const command = `create edge Follows from ${rIdA} to ${rIdB}`;

      try {
        let {result, tx} = await session.runInTransaction((tx)=>{
            return tx
            .command(`CREATE EDGE Follows from ${rIdA} to ${rIdB}`)
            .one();
        });

        // console.log(result);
        // console.log(tx);
        // const edge = await session.create("EDGE", "Follows")
        // .from(rIdA).to(rIdB)
        // .one()

        resolve(result);
      } catch (e) {
        console.log(e);
        reject(e);
      }
    });
  }

  // const results = lines.slice(0, 2).map(line => {
  const results = asyncMap(lines, async (line) => {
    const [a, b] = line.split(' ');
    // console.log(`Batch ${index}, Linking ${a} to ${b}`);
    return await link(a, b);
  });

  return results;
}


async function main() {
  let client, session, fileData;
  try {

    fileData = fs.readFileSync('data/social-data', {encoding: 'utf-8'});


    client = await OrientDBClient.connect({
      host: ORIENT_HOST,
      port: ORIENT_PORT
      // servers: [{ host: "localhost", port: 2425 }]
    });

    console.log("Connected");

    try {
      await client.createDatabase({
       name: DB_NAME,
       username: ORIENT_USER,
       password: ORIENT_PASS
      });
    } catch (e) {
        console.log('\n%s\n',e.message);
    }

    try {
          let pool = await client.sessions({
            name: DB_NAME,
            username: ORIENT_USER,
            password: ORIENT_PASS,
            pool: {max: PROCESSORS}
          });
          // acquire a session
          let session = await pool.acquire();
          // use the session

          console.log("Session open");
          //close/release session

          await session.command("create class User if not exists extends V");
          await session.command("create property User.id if not exists Integer (mandatory true)");
          await session.command("CREATE INDEX User.id IF NOT EXISTS UNIQUE");

          await session.command("create class Follows if not exists extends E");

          // clean db data
          await session.command("delete edge Follows");
          await session.command("delete vertex User");


          // let results = await session
          // .command("UPDATE User SET id = :id UPSERT WHERE id = :id; SELECT from User where id = :id limit 1", { params: { id: 1 }})
          // .all();
          //
          //
          // console.log('\n\n\n\n\n');
          // console.log(results);
          // console.log('\n\n\n\n\n');

          await session.close();


          const lines = fileData.toString().split('\n');
          const chunks = _.chunk(lines, lines.length/PROCESSORS);

          await Promise.all(chunks.map((chunk, index) => process(pool, chunk, index)));

          console.log("Done");
          // close pool
          await pool.close();
        } catch (e) {
          console.log(e.message);
        }

    // let results = await session
    // .command("insert into User set id = :id", { params: { id: 1 } })
    // .all();

  } catch (e) {
    console.log(e.message);


  if (client) {
    await client.close();
    console.log("Disconnected");
  }
};


main();
