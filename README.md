# Graph oriented database to analyze Twitter data using Orientdb

# Pre-requisites

- docker
- node v10+
- npm v6.4+

# Usage

To get latest version of orientdb docker image
    
    $ docker pull orientdb

Run orientdb container

    $ docker run -d --name orientdb -p 2424:2424 -p 2480:2480 -e ORIENTDB_ROOT_PASSWORD=rootpwd orientdb /orientdb/bin/server.sh
    
> **Note:** root is the default user and use the password provided in ORIENTDB_ROOT_PASSWORD

Install dependencies

    $ npm install
    
Run publisher file

    $ node social.js